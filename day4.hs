
import Data.List.Split
import Data.List
import qualified Data.Set as Set

valid :: [String] -> Bool
valid pass = (length pass) == (Set.size . Set.fromList) pass

wordSort input = map Data.List.sort (words input)

numValid inFn = length . filter valid . map inFn . lines

numValid1 = numValid words
numValid2 = numValid wordSort

nmain file fun = do
    inp <- readFile file
    putStrLn $ show $ fun inp
