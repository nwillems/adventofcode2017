
import Data.List.Split

lineDiff :: (Ord a, Num a) => [a] -> a
lineDiff xs = (maximum xs) - (minimum xs)

checksum :: ([Int] -> Int) -> [[Int]] -> Int
checksum f = sum . map f

-- Notes:
-- Make all pairs, filter on remainder == 0, hope for unique result
-- divide numbers and return result
divisorz' :: Int -> [Int] -> [Int] -> Int
divisorz' c [] (y:ys) = divisorz' y ys ys
divisorz' c (x:xs) ys 
    | c `rem` x == 0 = c `div` x
    | x `rem` c == 0 = x `div` c
    | otherwise      = divisorz' c xs ys

divisorz :: [Int] -> Int
divisorz (x:xs) = divisorz' x xs xs
-- divisorz xs = head . filter (\(x,y) -> (x `rem` y) == 0) pairs
--    where pairs = 

checksum1 = checksum lineDiff

checksum2 :: [[Int]] -> Int
checksum2 = checksum divisorz

-- Input parsing logic
lineParse :: String -> [Int]
lineParse = map read . splitOn "\t"
inputParse :: String -> [[Int]]
inputParse str = map lineParse $ lines str

nmain f chk = do
    inp <- readFile f
    putStrLn $ show $ (chk . inputParse) inp

-- Run the following
-- nmain "./day2.input" checksum
-- nmain "./day2.input" checksum2
