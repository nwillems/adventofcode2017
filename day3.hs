
ex n = 1 + n * (4*n +4)

layerWidth n = 2 * n  + 1

layer' x n
    | x > (ex n) = layer' x (n+1)
    | otherwise = n

layer x = layer' x 0

-- Below fn's expects a layer number
lowerRight n = ex n
lowerLeft n = (lowerRight n) - ((layerWidth n) -1)
upperLeft n = (lowerLeft n) - ((layerWidth n) -1)
upperRight n = (ex (n-1)) +1 + (layerWidth (n-1))

-- (x,y)
coordinates x
    | ll < x = ( x_layer, x_layer - ((ex x_layer) - x)) -- Bottom
    | ul < x = ( 0, x_layer) -- Left
    | ur < x = ( -1 * (x_layer), 0) -- Top
    | otherwise = (0, -1 * (x_layer)) -- Right
    where x_layer = layer x
          ll = lowerLeft x_layer
          ul = upperLeft x_layer
          ur = upperRight x_layer

distance (x,y) = abs x + abs y
